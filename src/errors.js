export default {
    charErr: "Invalid input!",
    lenErr: "Key must have length of 16",
    modeErr: "You haven't picked any mode",
    emptyErr: "None of inputs can be empty"
}